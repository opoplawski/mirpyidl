# -*- coding: utf-8 -*-
# ==============================================================================
"""
author
======

Novimir Antoniuk Pablant
- npablant@pppl.gov
- novimir.pablant@amicitas.com
"""
# ==============================================================================

import logging
import argparse

# This allows testing to be done by running python tests/examples.py from the
# top level project directory.
import sys
sys.path.append(".")

def test_example_01(use_idlrpc=False):

    print('\n'+'-'*80)
    print('Starting test_example_01')
    
    # Import mirpyidl.
    if use_idlrpc:
        import mirpyidlrpc as idl
    else:
        import mirpyidl as idl

    # Execute a command in IDL.
    # This will print 'Hello mom!' in the idlrpc window.
    idl.execute("PRINT, 'Hello mom!'")
    
    print('test_example_01 sucessful.')  

    
def test_example_02(use_idlrpc=False):
    
    print('\n'+'-'*80)
    print('Starting test_example_02')
    
    import numpy as np
    if use_idlrpc:
        import mirpyidlrpc as idl
    else:
        import mirpyidl as idl

    # Create a numpy array in python.
    py_array = np.random.normal(size=1000)

    # Copy this data to IDL.
    idl.setVariable('idl_array', py_array)

    # Calculate the standard devation and the mean in IDL.
    idl.execute('idl_stddev = STDDEV(idl_array)')
    idl.execute('idl_mean = MEAN(idl_array)')

    # Copy the results back to python.
    py_stddev = idl.getVariable('idl_stddev')
    py_mean = idl.getVariable('idl_mean')

    # Print out the results.
    print('Mean: {}, StdDev: {}'.format(py_mean, py_stddev))

    assert py_mean > -0.1 and py_mean < 0.1, 'py_mean is out of range.'
    assert py_stddev > 0.9 and py_stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_02 sucessful.')

    
def test_example_03(use_idlrpc=False):
    
    print('\n'+'-'*80)
    print('Starting test_example_03')
    
    import numpy as np
    if use_idlrpc:
        import mirpyidlrpc as idl
    else:
        import mirpyidl as idl

    # Create a numpy array in python.
    py_array = np.random.normal(size=1000)

    # Calculate the standard devication and mean using IDL.
    py_stddev = idl.callFunction('STDDEV', [py_array])
    py_mean = idl.callFunction('MEAN', [py_array])

    # Print out the results.
    print('Mean: {}, StdDev: {}'.format(py_mean, py_stddev))
    
    assert py_mean > -0.1 and py_mean < 0.1, 'py_mean is out of range.'
    assert py_stddev > 0.9 and py_stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_03 sucessful.')    


def test_example_04(use_idlrpc=False):
    
    print('\n'+'-'*80)
    print('Starting test_example_04')
    
    import numpy as np
    if use_idlrpc:
        from mirpyidlrpc import PyIDL
    else:
        from mirpyidl import PyIDL
        
    idl = PyIDL()

    # Create a numpy array in python.
    py_array = np.random.normal(size=1000)

    # Calculate the standard devication and mean using IDL.
    py_stddev = idl.callFunction('STDDEV', [py_array])
    py_mean = idl.callFunction('MEAN', [py_array])
    
    assert py_mean > -0.1 and py_mean < 0.1, 'py_mean is out of range.'
    assert py_stddev > 0.9 and py_stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_04 sucessful.')  

    
def test_example_05(use_idlrpc=False):
    
    print('\n'+'-'*80)
    print('Starting test_example_05')

    import numpy as np
    import example_05_idlmath

    array = np.random.normal(size=1000)

    # Here we transparently call the wrapped IDL functions.
    mean = example_05_idlmath.mean(array)
    stddev = example_05_idlmath.stddev(array)
    
    assert mean > -0.1 and mean < 0.1, 'py_mean is out of range.'
    assert stddev > 0.9 and stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_05 sucessful.')  

    
def test_example_06(use_idlrpc=False):
    
    print('\n'+'-'*80)
    print('Starting test_example_06')

    import numpy as np
    import example_06_idlmath

    array = np.random.normal(size=1000)

    # Here we transparently call the wrapped IDL functions.
    mean = example_06_idlmath.mean(array)
    stddev = example_06_idlmath.stddev(array)
    
    assert mean > -0.1 and mean < 0.1, 'py_mean is out of range.'
    assert stddev > 0.9 and stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_06 sucessful.')

    
def test_example_07(use_idlrpc=False):
    
    print('\n'+'-'*80)
    print('Starting test_example_07')

    import numpy as np
    import example_07_objects

    obj = example_07_objects.IdlContainer()
    count = obj.Count()
    
    assert count == 0, 'Incorrect object count.'
    
    print('test_example_07 sucessful.')   

    
def test_initialization(use_idlrpc=False):
    """
    Check that only the appropriate backend was used.

    This should be run after all the other tests are complete.
    """
        
    print('\n'+'-'*80)
    print('Starting test_initialization')
    
    import mirpyidl
        
    if use_idlrpc:
        assert mirpyidl.m_idlrpc_initialized == True, 'IDLRPC was not initialized.'
        assert mirpyidl.m_idl_initialized == False, 'Callable IDL was initialized when only IDLRPC sholud have been used.'
    else:
        assert mirpyidl.m_idl_initialized == True, 'Callable IDL was not initialized.'
        assert mirpyidl.m_idlrpc_initialized == False, 'IDLRPC was initialized when only Callable IDL sholud have been used.'

    print('test_initializaiton sucessful.')  

    
def setupLogging(debug=False):
    # Setup the logging output.
    if debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
        
    logging.basicConfig(
        format='[%(asctime)s %(levelname)-6.06s:%(name)-15.015s]  %(message)s'
        ,datefmt= '%H:%M:%S'
        ,level=level)
    
    # Import mirpyidl.
    if args.idlrpc:
        import mirpyidlrpc as mirpyidl
    else:
        import mirpyidl

    if args.debug:
        mirpyidl.setLoggingLevel(level)

        
def run(use_idlrpc=False):
       
    # Run all the tests using CallableID
    if use_idlrpc:
        print('\n\nRunning mirpyidlrpc tests.')
    else:
        print('\n\nRunning mirpyidl tests.')
        
    # Run all the tests using IDLRPC
    test_example_01(use_idlrpc)  
    test_example_02(use_idlrpc)
    test_example_03(use_idlrpc)
    test_example_04(use_idlrpc)
    test_example_05(use_idlrpc)
    test_example_06(use_idlrpc)    
    test_example_07(use_idlrpc)
    test_initialization(use_idlrpc)
     
    
if __name__ == '__main__':

    parser = argparse.ArgumentParser("""Run all the examples for mirpyidl""")
  
    parser.add_argument('--idlrpc'
                        ,action='store_true'
                        ,help="Use idlrpc for all tests.")
    parser.add_argument('--debug'
                        ,action='store_true'
                        ,help="Turn on debugging output.") 
    
    args = parser.parse_args()

    setupLogging(args.debug)

    # Run the tests.
    run(args.idlrpc)
