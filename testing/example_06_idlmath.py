# idlmath.py

import sys

if '--idlrpc' in sys.argv:
    import mirpyidlrpc as idl
else:
    import mirpyidl as idl

def stddev(*args, **kwargs):
    return idl.callFunction('STDDEV', args, kwargs)

def mean(*args, **kwargs):
    return idl.callFunction('MEAN', args, kwargs)
