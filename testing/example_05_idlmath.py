# idlmath.py

import sys

if '--idlrpc' in sys.argv:
    import mirpyidlrpc as idl
else:
    import mirpyidl as idl
    
def stddev(input):
    return idl.callFunction('STDDEV', [input])

def mean(input):
    return idl.callFunction('MEAN', [input])
