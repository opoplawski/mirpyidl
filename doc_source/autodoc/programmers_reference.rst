
********************************
 mirpyidl programmers reference
********************************

This shows the documentaiton for the mirpyidl public interface.

.. automodule:: mirpyidl
    :members: execute 
              ,ex
              ,setVariable
              ,set
              ,getVariable
              ,get
              ,callFunction
              ,callPro
    :undoc-members:
    :private-members:
    :show-inheritance:
    :noindex:

.. autoclass:: PyIDL
    :members:
    :inherited-members:
    :undoc-members:
    :private-members:
    :show-inheritance:
    :noindex:

.. autoclass:: PyIDLObject
    :members:
    :inherited-members:
    :undoc-members:
    :private-members:
    :show-inheritance:
    :noindex:
