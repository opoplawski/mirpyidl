# _IdlContainer.py

import sys

if '--idlrpc' in sys.argv:
    from mirpyidlrpc import PyIDLObject
else:
    from mirpyidl import PyIDLObject

class IdlContainer(PyIDLObject):

    # Define the the IDL command needed to create the object.
    _creation_command = "OBJ_NEW"
    _creation_params = ['IDL_Container']
    _creation_keywords = None

    def Count(self, *args, **kwargs):
        return self.callMethodFunction('Count', args, kwargs)
