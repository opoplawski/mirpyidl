
***************************
 mirpyidl public interface
***************************

This shows the documentaiton for the mirpyidl public interface.

.. automodule:: mirpyidl
    :members: execute 
              ,ex
              ,setVariable
              ,set
              ,getVariable
              ,get
              ,callFunction
              ,callPro
    :undoc-members:
    :show-inheritance:

.. autoclass:: PyIDL
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: PyIDLObject
    :members:
    :undoc-members:
    :show-inheritance:
